#!/bin/sh
set -e
. ./build.sh
 
mkdir -p isodir
mkdir -p isodir/boot
mkdir -p isodir/boot/grub
 
cp sysroot/boot/ottos.kernel isodir/boot/ottos.kernel
cat > isodir/boot/grub/grub.cfg << EOF
menuentry "ottos" {
    multiboot /boot/ottos.kernel
}
EOF
grub-mkrescue -o ottos.iso isodir