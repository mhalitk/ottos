#include <stdio.h>

#if defined(__is_ottos_kernel)
#include <kernel/tty.h>
#endif

int putchar(int intChar)
{
#if defined(__is_ottos_kernel)
    char c = (char) intChar;
    terminal_write(&c, sizeof(c));
#else
    // todo: need to implement a write system call
#endif
    return intChar;
}