#include <string.h>

void* memcpy(void* restrict destinationPtr, const void* restrict sourcePtr, size_t size)
{
    unsigned char* destination = (unsigned char*) destinationPtr;
    const unsigned char* source = (const unsigned char*) sourcePtr;
    for (size_t i = 0; i < size; ++i)
        destination[i] = source[i];
    return destinationPtr;
}