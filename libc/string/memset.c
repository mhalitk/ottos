#include <string.h>

void* memset(void* bufferPtr, int value, size_t size)
{
    unsigned char* buffer = (unsigned char*) bufferPtr;
    for (size_t i = 0; i < size; ++i) {
        buffer[i] = value;
    }
    return bufferPtr;
}