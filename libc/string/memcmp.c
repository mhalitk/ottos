#include <string.h>

int memcmp(const void* first, const void* second, size_t size)
{
    const unsigned char* cmp1 = (const unsigned char*) first;
    const unsigned char* cmp2 = (const unsigned char*) second;
    for (size_t i = 0; i < size; ++i) {
        if (cmp1[i] < cmp2[i])
            return -1;
        else if (cmp1[i] > cmp2[i])
            return 1;
    }
    return 0;
}