#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <kernel/vga.h>

size_t terminalRow;
size_t terminalColumn;
uint8_t terminalColor;
uint16_t* terminalBuffer;

void terminal_initialize(void)
{
    terminalRow = 0;
    terminalColumn = 0;
    terminalColor = make_color(COLOR_WHITE, COLOR_MAGENTA);
    terminalBuffer = VGA_MEMORY;
    for (size_t y = 0; y < VGA_HEIGHT; ++y) {
        for(size_t x = 0; x < VGA_WIDTH; ++x) {
            const size_t index = y * VGA_WIDTH + x;
            terminalBuffer[index] = make_vgaentry(' ', terminalColor);
        }
    }
}

void terminal_setcolor(uint8_t newColor) 
{
    terminalColor = newColor;
}

void terminal_putentryat(char c, uint8_t color, size_t x, size_t y)
{
    const size_t index = y * VGA_WIDTH + x;
    terminalBuffer[index] = make_vgaentry(c, color);
}

void terminal_scrolldownbyone()
{
    for (size_t row = 0; row < VGA_HEIGHT; ++row) {
        for (size_t column = 0; column < VGA_WIDTH; ++column) {
            const size_t oldIndex = (row+1) * VGA_WIDTH + column;
            const size_t newIndex = row * VGA_WIDTH + column;
            terminalBuffer[newIndex] = terminalBuffer[oldIndex];
        }
    }
}


void terminal_putchar(char c)
{
    if (c == '\n') {
        terminalColumn = 0;
        if (terminalRow == VGA_HEIGHT-1) {
            terminal_scrolldownbyone();
        } else {
            terminalRow++;
        }
        return;
    }

    terminal_putentryat(c, terminalColor, terminalColumn, terminalRow);

    if (++terminalColumn == VGA_WIDTH) {
        terminalColumn = 0;
        if (terminalRow == VGA_HEIGHT-1) {
            terminal_scrolldownbyone();
        } else {
            terminalRow++;
        }
    }

}

void terminal_write(const char* data, size_t size)
{
    for (size_t i = 0; i < size; ++i)
        terminal_putchar(data[i]);
}

void terminal_writestring(const char* data)
{
    terminal_write(data, strlen(data));
}