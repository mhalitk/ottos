#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#if UINT32_MAX == UINTPTR_MAX
#define STACK_CHK_GUARD 0xB5842548
#else
#define STACK_CHK_GUARD 0xA584686874254878
#endif

uintptr_t __stack_chk_guard = STACK_CHK_GUARD;

__attribute__((__noreturn__))
void __stack_chk_fail(void)
{
#if __STDC_HOSTED__
    abort();
#elif __is_ottos_kernel
    // todo write panic here
    // printf("Panic laaan!");
    abort();
#endif
}